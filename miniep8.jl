function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function letras(x)

    if x == "J"
        return 11
    elseif x == "Q"
        return 12
    elseif x == "K"
        return 13
    elseif x == "A"
        return 14
    else
        return parse(Int,x)
    end
end

function compareByValue(x,y)
    aux1 = letras(SubString(x,1,lastindex(x)-1))
    aux2 = letras(SubString(y,1,lastindex(y)-1))
    if aux1 >= aux2
        return false
    else
        return true
    end
end

function naipes(x)
    if x == "♦"
        return 1
    elseif x == "♠"
        return 2
    elseif x == "♥"
        return 3
    elseif x == "♣"
        return 4
    end
end

function compareByValueAndSuit(x,y)
    aux1 = naipes(SubString(x,lastindex(x),lastindex(x)))
    aux2 = naipes(SubString(y,lastindex(y),lastindex(y)))

    if aux2>aux1
        return true
    elseif aux1==aux2
        return compareByValue(x,y)
    else
        return false
    end

end
